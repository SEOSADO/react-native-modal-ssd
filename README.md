# react-native-modal-ssd

## Getting Started

### Installing
```json
"react-native-modal-ssd": "https://gitlab.com/SEOSADO/react-native-modal-ssd.git"
```

## Sample Code

```javascript
import React, {useEffect, useRef, useState} from 'react';
import {View, TouchableOpacity, FlatList, Text} from 'react-native';
import SSD_Modal from 'react-native-modal-ssd';

const App = () => {
  const [useModal, setUseModal] = useState(false);

  const data = [
    {id: 1, name: 'data1'},
    {id: 2, name: 'data2'},
    {id: 3, name: 'data3'},
    {id: 4, name: 'data4'},
    {id: 5, name: 'data5'},
    {id: 6, name: 'data6'},
  ];
  const renderItem = ({item, index}) => {
    return (
      <View style={{marginLeft: 20}}>
        <TouchableOpacity
          style={{height: 50, width: 150, backgroundColor: 'red'}}
          onPress={() => console.log(item.name)}
        />
        <Text
          style={{
            height: 20,
            width: '100%',
            color: 'black',
          }}>
          {item.id}
        </Text>
      </View>
    );
  };
  return (
    <View>
      {/* modal open */}
      <TouchableOpacity
        style={{height: 50, width: 50, backgroundColor: 'red'}}
        onPress={() => setUseModal(true)}
      />
      {/* modal */}
      <SSD_Modal
        data={data}
        visible={useModal}
        setVisible={setUseModal}
        renderItem={renderItem}
        // titleName={'Title'}
        // titleStyle={}
        // titleFontStyle={{height: 40, color: 'red'}}
        // isBottomScrollDisable={true}
        // isCloseBackgroundClickDisable={true}
        // duration={10}
        // isTitleDisable={true}
        // modalProps={{transparent: true}}
        // flatListProps={{}}
      />
    </View>
  );
};

export default App;
```

|**Name**|**Type**|**Default**|**Description**|
|------|---|---|---|
|data                         |array||[{data:1}]|
|visible                      |bool||useState use
|setVisible                   |func||useState set|
|renderItem                   |func||render view|
|titleName                    |string||title 이름|
|titleStyle                   |style||title 사용 시 title 스타일|
|titleFontStyle               |style||title 사용 시 font 스타일|
|isBottomScrollDisable        |bool|false|모달 sheet scroll시 close|
|isCloseBackgroundClickDisable|bool|false|배경 이미지 클릭 시 모달 close|
|isTitleDisable               |bool|false|title 사용여부|
|duration                     |number|350|animation speed|
|modalProps                   |props||react-native-modal props참조|
|flatListProps                |props||FlatList props참조|





## License