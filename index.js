import React, {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Dimensions,
  PanResponder,
  FlatList,
  ScrollView,
  StatusBar,
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import {defaultStyles} from './styles';

export default function SSD_Modal(props, state) {
  // Hooks
  const [modalVisible, setModalVisible] = useState(true);

  // Props
  const {
    data,
    visible,
    setVisible,
    renderItem,
    titleName,
    titleStyle,
    titleFontStyle,
    isBottomScrollDisable = false, // Default : false
    // isCloseButtonDisable = false, // Default : false
    isCloseBackgroundClickDisable = false, // Default : false
    isTitleDisable = false, // Default : false
    duration = 350,
    modalProps,
    flatListProps,
  } = props;

  // -->> Animated
  const screenHeight = Dimensions.get('screen').height;
  const panY = useRef(new Animated.Value(screenHeight)).current;
  const translateY = panY.interpolate({
    inputRange: [-1, 0, 1],
    outputRange: [0, 0, 1],
  });

  const resetBottomSheet = Animated.timing(panY, {
    toValue: 0,
    duration: duration,
    useNativeDriver: true,
  });

  const closeBottomSheet = Animated.timing(panY, {
    toValue: screenHeight,
    duration: duration,
    useNativeDriver: true,
  });

  const panResponders = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => false,
      onPanResponderMove: (event, gestureState) => {
        panY.setValue(gestureState.dy);
      },
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dy > 0 && gestureState.vy > 1.5) {
          closeModal();
        } else {
          resetBottomSheet.start();
        }
      },
    }),
  ).current;
  // Animated <<--

  // Close
  const closeModal = () => {
    closeBottomSheet.start(() => {
      setModalVisible(false);
      setVisible(false);
    });
  };

  // Navigation Stack or Modal
  if ('Navigation' === 'Navigation') {
    useEffect(() => {
      if (visible) {
        resetBottomSheet.start();
      }
    }, [visible]);
  }
  if ('Modal' === 'Modal') {
    useEffect(() => {
      if (modalVisible) {
        resetBottomSheet.start();
      }
    }, [modalVisible]);
  }

  return (
    <Modal
      // visible={modalVisible}
      visible={visible}
      animationType={'fade'} //'none', 'slide', 'fade'
      animationIn="bounceInDown"
      transparent
      statusBarTranslucent
      style={defaultStyles.modalSt}
      {...modalProps}>
      <View style={defaultStyles.overlay}>
        {/* Close Button */}
        {isCloseBackgroundClickDisable ? (
          <TouchableWithoutFeedback onPress={closeModal}>
            <View style={defaultStyles.backgroundZone} />
          </TouchableWithoutFeedback>
        ) : (
          <></>
        )}

        {/* Title */}
        {isTitleDisable ? (
          <Animated.View
            style={{
              ...defaultStyles.bottomSheetContainer,
              transform: [{translateY: translateY}],
            }}
            {...panResponders.panHandlers}>
            <View style={[defaultStyles.topHeader, titleStyle]}>
              <Text style={[defaultStyles.topHeaderUsername, titleFontStyle]}>
                {titleName}
              </Text>
            </View>
          </Animated.View>
        ) : (
          <Animated.View
            style={[
              {
                ...defaultStyles.bottomSheetContainer2,
                transform: [{translateY: translateY}],
              },
            ]}
            {...panResponders.panHandlers}>
            <View style={defaultStyles.topBar} />
          </Animated.View>
        )}

        {/* Item */}
        <Animated.View
          style={{transform: [{translateY: translateY}]}}
          {...(isBottomScrollDisable === true
            ? {...panResponders.panHandlers}
            : {})}>
          <View style={defaultStyles.bottomSheetList}>
            <FlatList
              data={data}
              keyExtractor={(item, index) => String(index)}
              renderItem={renderItem}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              {...flatListProps}
            />
          </View>
        </Animated.View>
      </View>
    </Modal>
  );
}
