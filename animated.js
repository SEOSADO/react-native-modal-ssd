import React, {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Dimensions,
  PanResponder,
  FlatList,
  ScrollView,
  StatusBar,
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

// 애니메이션
const screenHeight = Dimensions.get('screen').height;
const panY = useRef(new Animated.Value(screenHeight)).current;
export const translateY = panY.interpolate({
  inputRange: [-1, 0, 1],
  outputRange: [0, 0, 1],
});

export const resetBottomSheet = Animated.timing(panY, {
  toValue: 0,
  duration: 50,
  useNativeDriver: true,
});

export const closeBottomSheet = Animated.timing(panY, {
  toValue: screenHeight,
  duration: 50,
  useNativeDriver: true,
});

export const panResponders = useRef(
  PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onMoveShouldSetPanResponder: () => false,
    onPanResponderMove: (event, gestureState) => {
      panY.setValue(gestureState.dy);
    },
    onPanResponderRelease: (event, gestureState) => {
      if (gestureState.dy > 0 && gestureState.vy > 1.5) {
        closeModal();
      } else {
        resetBottomSheet.start();
      }
    },
  }),
).current;
