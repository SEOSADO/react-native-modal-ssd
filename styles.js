import {StyleSheet, Dimensions, PixelRatio} from 'react-native';

// 추후 공통 모듈로 빼기?
const {width, height} = Dimensions.get('window');
const dimensions = width < height ? width : height;
const guidelineBaseWidth = 360; // 우선 피그마 UI 틀 사이즈로 넣었음.
const guidelineBaseHeight = 740; // 우선 피그마 UI 틀 사이즈로 넣었음.
const scale = size => {
  return PixelRatio.roundToNearestPixel(
    (dimensions / guidelineBaseWidth) * size,
  );
};
const verticalScale = size => (height / guidelineBaseHeight) * size;

export const defaultStyles = StyleSheet.create({
  modalSt: {
    margin: scale(0),
  },
  overlay: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
  },
  bottomSheetContainer: {
    backgroundColor: 'white',
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
    height: scale(50),
  },
  bottomSheetContainer2: {
    backgroundColor: 'white',
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
    height: scale(25),
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundZone: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  topHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: scale(10),
    paddingHorizontal: scale(20),
  },
  topBar: {
    backgroundColor: 'gray',
    height: scale(5),
    width: scale(50),
    borderRadius: scale(5),
  },
  topHeaderUsername: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: '#000000',
  },
  bottomSheetList: {
    height: scale(300),
    backgroundColor: 'white',
  },
});
